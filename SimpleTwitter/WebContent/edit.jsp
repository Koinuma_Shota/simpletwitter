<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.account}のつぶやき編集</title>
<link href="css/style.css" rel="stylesheet" type="text/css">

<!-- jQueryの追加 -->
<script src="./js/jquery-3.6.0.min.js"></script>
<script src="./js/main.js"></script>

</head>
<body>
	<div class="main-contents">

		<!--エラーメッセージの表示-->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


		<form action="edit" method="post" id="edit">
			<input type="hidden" name="messageId" value="${message.id}" /> <br />
			<textarea name="text" cols="35" rows="5" class="tweetbox"><c:out
					value="${message.text}" /></textarea>

			<br /> <input type="submit" value="更新" />(140文字まで) <br />
			<a href="./">戻る</a>
		</form>

	</div>
</body>
</html>