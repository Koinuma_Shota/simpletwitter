//つぶやき削除ダイアログ
$(document).ready(function(){
	$('#delete').on('click', function(){
		if(!confirm('本当に削除しますか？')){
			return false;
		} else {
			alert('削除しました');
		}
	});

//つぶやき編集ダイアログ
	$('#edit').on('submit', function(){
		if(!confirm('この内容で更新しますか？')){
			return false;
		} else {
			alert('更新しました');
		}
	});

//ユーザー情報更新ダイアログ
	$('#setting').on('submit', function(){
		if(!confirm('アカウント情報を更新しますか？')){
			return false;
		} else {
			alert('更新しました');
		}
	});
});