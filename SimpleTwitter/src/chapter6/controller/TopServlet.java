/*トップ画面を表示する為のクラス。
 * 表示内容はほかのクラスに処理依頼し、処理結果をjspに返す。
 */


package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isShowMessageForm = false;
		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}

		String userId = request.getParameter("user_id");

		//開始日、終了日の取得
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		//リストに格納してServiceに渡す
		List<UserMessage> messages = new MessageService().select(userId, startDate, endDate);
		List<UserComment> comments = new CommentService().select();

		request.setAttribute("messages", messages);

		//リクエストスコープに"comments"インスタンスを保存
		request.setAttribute("comments", comments);

		//リクエストスコープに"startDate""endDate"インスタンスを保存
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);

		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}