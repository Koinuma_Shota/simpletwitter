//ログインフィルター

package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//URLを指定
@WebFilter(urlPatterns = {"/setting", "/edit"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {


		//変数"request,responce"をHttp型へキャストする
		HttpServletRequest loginrequest = (HttpServletRequest)request;
		HttpServletResponse loginresponse =(HttpServletResponse) response;

		//セッションを取得する
		HttpSession session = loginrequest.getSession();

		//ログインしているか判断し、ログインしていたらリクエストのあった画面へ遷移
		if(session.getAttribute("loginUser") != null){

			//サーブレットの実行（画面遷移)
			chain.doFilter(request, response);

			//ログインしていなかったらエラーメッセージを表示し、ログイン画面へ遷移
		}else {

			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインして下さい");
			session.setAttribute("errorMessages", errorMessages);
			loginresponse.sendRedirect("login.jsp");
		}
	}

	@Override
	public void init(FilterConfig filterConfig){
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}
}