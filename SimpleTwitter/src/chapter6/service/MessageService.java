package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String startDate, String endDate) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();



			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			//終了日のデフォルト値設定
			Date now = new Date();//現在時刻の取得
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//フォーマットを指定
			String defaultEnd = df.format(now);


			//開始日・終了日の入力判定
			if(!StringUtils.isEmpty(startDate)){
				startDate += " 00:00:00";
			}else {
				//開始日のデフォルト値
				startDate = "2020/01/01 00:00:00";
			}
			if(!StringUtils.isEmpty(endDate)) {
				endDate += " 23:59:59";
			}else {
				endDate = defaultEnd;
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
			commit(connection);
			return messages;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//メッセージを削除するためのメソッド
	public void delete(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			// MessageDaoにdeleteメソッドを渡す
			new MessageDao().delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//メッセージを編集するためのメソッド
	public void update(int id, String text) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, id, text);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//編集id取得のためのメソッド
	public Message select(int id) {

		Connection connection = null;
		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, id);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}





